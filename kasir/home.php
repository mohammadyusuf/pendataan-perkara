<div class="row purchace-popup">
    <div class="col-5">
        <span class="d-block d-md-flex align-items-center">
            <p class="mr-2">Selamat Datang Petugas Kasir</p>
            <a class="btn purchase-button mt-4 mt-md-0" href="#"><?= $detail['nama'] ?></a>
            <i class="mdi mdi-close popup-dismiss d-none d-md-block"></i>
        </span>
    </div>
</div>

<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
            <div class="card-body">
                <div class="clearfix">
                    <div class="float-left">
                        <i class="mdi mdi-cube text-warning icon-lg"></i>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 text-right">Perkara Belum Terbayar</p>
                        <?php $q = $koneksi->query("SELECT * FROM tbl_pembayaran WHERE status_bayar = 'belum'");
                        $jml = $q->num_rows;
                        ?>
                        <div class="fluid-container">
                            <h3 class="font-weight-medium text-right mb-0"><?= $jml ?></h3>
                        </div>
                    </div>
                </div>
                <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> selengkpanya
                </p>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
            <div class="card-body">
                <div class="clearfix">
                    <div class="float-left">
                        <i class="mdi mdi-cash text-success icon-lg"></i>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 text-right">Perkara Sudah Terbayar</p>
                        <?php $q = $koneksi->query("SELECT * FROM tbl_pembayaran WHERE status_bayar = 'sudah'");
                        $jml = $q->num_rows;
                        ?>
                        <div class="fluid-container">
                            <h3 class="font-weight-medium text-right mb-0"><?= $jml ?></h3>
                        </div>
                    </div>
                </div>
                <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> selengkpanya
                </p>
            </div>
        </div>
    </div>
</div>