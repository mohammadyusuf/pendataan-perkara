<?php
session_start();
require '../config/koneksi.php';
require '../config/fungsi.php';

if ($_SESSION['kode_user'] == "") {
    header("location:../index.php");
}
if ($_SESSION['level'] == "pelayanan") {
    header("location:../pelayanan/beranda.php");
}
if ($_SESSION['level'] == "kepala") {
    header("location:../kepala/beranda.php");
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Pendataan Perkara</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="../assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="../assets/vendors/css/vendor.bundle.addons.css">
    <link href="../assets/vendors/dataTable/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="../assets/css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="../assets/images/favicon.png" />
</head>

<body>
    <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
        <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
                <a class="navbar-brand brand-logo" href="index.html">
                    <!-- <b>Pengadilan</b><small>Agama</small> -->
                    <img src="../assets/images/logo.png" alt="logo" />
                </a>
                <a class="navbar-brand brand-logo-mini" href="index.html">
                    <img src="../assets/images/bank.svg" alt="logo" />
                </a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center">
                <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item dropdown d-none d-xl-inline-block">
                        <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                            <?php
                            $user = $koneksi->query("SELECT * FROM tbl_user WHERE kode_user = '$_SESSION[kode_user]'");
                            $detail = mysqli_fetch_array($user);
                            ?>
                            <span class="profile-text">Hello, <?= $detail['nama'] ?>!</span>
                            <img class="img-xs rounded-circle" src="../assets/images/faces/face1.jpg" alt="Profile image">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                            <a class="dropdown-item p-0">
                                <div class="d-flex border-bottom">
                                    <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                                        <i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i>
                                    </div>
                                    <div class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
                                        <i class="mdi mdi-account-outline mr-0 text-gray"></i>
                                    </div>
                                    <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                                        <i class="mdi mdi-alarm-check mr-0 text-gray"></i>
                                    </div>
                                </div>
                            </a>
                            <a href='../logout.php' class="dropdown-item">
                                Sign Out
                            </a>
                        </div>
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                    <span class="mdi mdi-menu"></span>
                </button>
            </div>
        </nav>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial:partials/_sidebar.html -->
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item nav-profile">
                        <div class="nav-link">
                            <div class="user-wrapper">
                                <div class="profile-image">
                                    <img src="../assets/images/faces/face1.jpg" alt="profile image">
                                </div>
                                <div class="text-wrapper">
                                    <p class="profile-name"><?= $detail['nama'] ?></p>
                                    <div>
                                        <small class="designation text-muted"><?= ucfirst($detail['status']) ?></small>
                                        <span class="status-indicator online"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <?php @$menu = $_GET['menu'] ?>

                    <li class="nav-item <?php if ($menu == "") {
                                            echo 'active';
                                        } ?>">
                        <a class="nav-link" href="?menu">
                            <i class="menu-icon mdi mdi-television"></i>
                            <span class="menu-title">Beranda</span>
                        </a>
                    </li>
                    <li class="nav-item <?php if ($menu == "pembayaran" || $menu == 'ubah_status') {
                                            echo 'active';
                                        } ?>">
                        <a class="nav-link" href="?menu=pembayaran">
                            <i class="menu-icon mdi mdi-cash"></i>
                            <span class="menu-title">Pembayaran</span>
                        </a>
                    </li>

                </ul>
            </nav>
            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">

                    <?php

                    error_reporting(E_NOTICE ^ E_ALL);
                    switch ($_GET['menu']) {
                        case 'pembayaran':
                            include 'pembayaran/index.php';
                            break;
                        case 'ubah_status':
                            include 'pembayaran/ubah_status.php';
                            break;

                        default:
                            include 'home.php';
                            break;
                    }
                    ?>

                </div>
                <!-- content-wrapper ends -->
                <!-- partial:partials/_footer.html -->
                <footer class="footer">
                    <div class="container-fluid clearfix">
                        <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2019
                            <a href="http://www.bootstrapdash.com/" target="_blank">Pengadilan Agama Kudus</a>. All rights reserved.</span>
                        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                            <i class="mdi mdi-heart text-danger"></i>
                        </span>
                    </div>
                </footer>
            </div>
        </div>

    </div>

    <!-- plugins:js -->
    <script src="../assets/vendors/js/vendor.bundle.base.js"></script>
    <script src="../assets/vendors/js/vendor.bundle.addons.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="../assets/js/off-canvas.js"></script>
    <script src="../assets/js/misc.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="../assets/js/dashboard.js"></script>
    <!-- End custom js for this page-->
    <script src="../assets/js/jquery-3.1.1.min.js"></script>
    <script src="../assets/vendors/chosen-master/chosen.jquery.js"></script>
    <script src="../assets/vendors/dataTable/jquery.dataTables.min.js"></script>
    <script src="../assets/vendors/dataTable/dataTables.bootstrap4.min.js"></script>
    <script src="../assets/js/script.js"></script>
    <script src="../assets/js/panjar.js"></script>
    <script src="../assets/js/panjar2.js"></script>
    <script src="../assets/js/gugatan.js"></script>
    <script src="../assets/jquery-ui/jquery-ui.js"></script>
</body>

</html>