<div class="row">
    <div class="col-lg-9">
        <h1>
            Kasir
            <small>Pengadilan Agama</small>
        </h1>
    </div>
    <div class="col-lg-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-end">
                <li class="breadcrumb-item active"><a href="#">Pembayaran</a></li>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-striped" id="tabel">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Nomor Perkara</td>
                                <td>Tgl Daftar</td>
                                <td>Jenis Pekara</td>
                                <td>Para Pihak</td>
                                <td>Total Bayar</td>
                                <td>Status</td>
                                <td>Aksi</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $data = mysqli_query($koneksi, "SELECT * FROM tbl_data_pihak as A INNER JOIN tbl_permohonan_perkara as B ON A.kode_data=B.kode_data INNER JOIN tbl_sub_perkara as C ON B.kode_sub=C.kode_sub INNER JOIN tbl_pembayaran as L ON B.kode_permohonan=L.kode_permohonan");
                            $no = 1;
                            while ($p = mysqli_fetch_assoc($data)) :
                                ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td>
                                        <?= $p['nomor_perkara'] == NULL ? "-" : $p['nomor_perkara'] ?>
                                    </td>
                                    <td><?= $p['tgl_daftar_pemohon'] ?></td>
                                    <td><?= $p['nama_sub'] ?></td>
                                    <td>
                                        <?= $p['NIK_pemohon'] . " - " . $p['nama_pemohon'] ?> <br><br>
                                        <?= $p['nama_termohon'] == NULL ? "-" : $p['nama_termohon'] ?>
                                    </td>
                                    <td><?= $p['total_bayar'] ?></td>
                                    <td>
                                        <?php if ($p['status_bayar'] == 'belum') { ?>
                                            <span class='text-warning' id='belum'>Belum</span>
                                        <?php } else { ?>
                                            <span class='text-success' id='belum'>Sudah</span>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if ($p['nomor_perkara']) { ?>
                                            <input value="Bayar" type="button" class="btn btn-primary" disabled>
                                        <?php } else { ?>
                                            <a href="?menu=ubah_status&no=<?= $p['kode_pembayaran'] ?>" class="btn btn-primary">Bayar</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php endwhile; ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>