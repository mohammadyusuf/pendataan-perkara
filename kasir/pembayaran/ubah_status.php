<div class="row">
    <div class="col-lg-9">
        <h1>
            Kasir
            <small>Pengadilan Agama</small>
        </h1>
    </div>
    <div class="col-lg-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-end">
                <li class="breadcrumb-item"><a href="#">Pembayaran</a></li>
                <li class="breadcrumb-item active"><a href="#">Ubah Status</a></li>
            </ol>
        </nav>
    </div>
</div>
<?php
$id = $_GET['no'];
$query = $koneksi->query("SELECT * FROM tbl_data_pihak as A INNER JOIN tbl_permohonan_perkara as B ON A.kode_data=B.kode_data INNER JOIN tbl_sub_perkara as C ON B.kode_sub=C.kode_sub INNER JOIN tbl_perkara_perdata as J ON C.kode_perkara=J.kode_perkara INNER JOIN tbl_pembayaran as L ON B.kode_permohonan=L.kode_permohonan WHERE kode_pembayaran = $id");
$hasil = mysqli_fetch_assoc($query);

if ($hasil['kode_perkara'] == '1') {
    $kode = kode('G');
} else if ($hasil['kode_perkara'] == '2') {
    $kode = kode('P');
} else {
    $kode = kode('S');
}

?>

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3>Ubah Status</h3>
                <div class="col-lg-9 offset-lg-2">


                    <form action="" method="post">
                        <div class="form-group">
                            <label class="label">Tanggal Daftar</label>
                            <div class="input-group">
                                <input type="text" class="form-control" value="<?= $hasil['tgl_daftar_pemohon'] ?>" disabled required name="user">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="label">Pihak</label>
                            <div class="input-group">
                                <input type="text" value="<?= $hasil['NIK_pemohon'] . " - " . $hasil['nama_pemohon'] ?>" class="form-control" disabled required name="pass">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="label">Total Bayar</label>
                            <div class="input-group">
                                <input type="text" value="Rp. <?= number_format($hasil['total_bayar']) ?>" class="form-control" disabled required name="pass">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="label">Status Bayar</label>
                            <div class="input-group">
                                <select class="form-control" name="status" id="status">
                                    <option value="">-- Pilih --</option>
                                    <option value="belum">Belum Bayar</option>
                                    <option value="sudah">Sudah Bayar</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" id="kode" name="nomor" value="<?= $kode ?>">
                        <div class="form-group">
                            <label class="label">Nomor Perkara</label>
                            <div class="input-group">
                                <input type="text" id="nomor" class="form-control" disabled required name="po">
                            </div>
                        </div>
                        <a href="?menu=pembayaran" class="btn btn-warning">Kembali</a>
                        <input type="submit" name="bayar" value="Simpan" class="btn btn-info">
                    </form>
                    <?php
                    $nomor = $_POST['nomor'];
                    $status = $_POST['status'];

                    if (isset($_POST['bayar'])) {
                        $simpan = "UPDATE tbl_pembayaran SET nomor_perkara = '$nomor', status_bayar = '$status' WHERE kode_pembayaran = $id";

                        $cek = $koneksi->query($simpan);
                        if ($cek) { ?>
                            <script>
                                alert("Data berhasil Disimpan");
                                window.location.href = '?menu=pembayaran';
                            </script>
                        <?php } else { ?>
                            <script>
                                alert("Data Gagal");
                                window.location.href = '?menu=ubah_status&no=<?= $id ?>';
                            </script>
                        <?php }
                } ?>


                </div>
            </div>
        </div>
    </div>
</div>