<script src="../assets/vendors/js/vendor.bundle.base.js"></script>
<script src="../assets/vendors/js/vendor.bundle.addons.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="../assets/js/off-canvas.js"></script>
<script src="../assets/js/misc.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="../assets/js/dashboard.js"></script>
<!-- End custom js for this page-->
<script src="../assets/js/jquery-3.1.1.min.js"></script>
<script src="../assets/vendors/dataTable/jquery.dataTables.min.js"></script>
<script src="../assets/vendors/dataTable/dataTables.bootstrap4.min.js"></script>
<script src="../assets/js/script.js"></script>