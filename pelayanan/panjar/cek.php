<?php
$kode_permohonan = $_GET['no'];
$semua = $koneksi->query("SELECT * FROM tbl_radius as R INNER JOIN tbl_desa as D ON R.kode_radius=D.kode_radius INNER JOIN tbl_data_pihak as A ON D.kode_desa=A.kode_desa INNER JOIN tbl_permohonan_perkara as B ON A.kode_data=B.kode_data INNER JOIN tbl_sub_perkara as C ON B.kode_sub=C.kode_sub INNER JOIN tbl_user as K ON B.kode_user=K.kode_user WHERE kode_permohonan = $kode_permohonan");
$hasil = mysqli_fetch_array($semua);

$cekP = $koneksi->query("SELECT * FROM tbl_panjar WHERE kode_sub = $hasil[kode_sub] AND kode_radius = $hasil[kode_radius]");
$result = mysqli_fetch_array($cekP);

$hari_ini = tgl_indo(date('Y-m-d'));

$cekNomor = $koneksi->query("SELECT * FROM tbl_pembayaran WHERE kode_permohonan = '$kode_permohonan'");
$cekNomor2 = $cekNomor->num_rows;
?>

<!-- <div class="row">
    <div class="col-lg-9">
        <h1>
            Pelayanan
            <small>Pengadilan Agama</small>
        </h1>
    </div>
    <div class="col-lg-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-end">
                <li class="breadcrumb-item"><a href="#">Pembayaran</a></li>
                <li class="breadcrumb-item active"><a href="#">Cek</a></li>
            </ol>
        </nav>
    </div>
</div> -->
<style>
    hr {
        display: block;
        margin-top: 0.1em;
        margin-bottom: 0.5em;
        margin-left: auto;
        margin-right: auto;
        border-style: double;
        border-width: 2px;
    }

    table,
    th,
    td {
        padding: 8px 10px;
    }
</style>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-1 mb-2">
                        <img src="../assets/images/logo8.png" width="100px" alt="">
                    </div>
                    <div class="col-md-11 mt-2">
                        <center>
                            <h3>PENGADILAN AGAMA KUDUS</h3>
                            <h6>Jl. Raya Kudus - Pati KM 4 Kudus 59321</h6>
                            <p>Telp/Fax : (0291) 438385</p>
                        </center>
                    </div>
                </div>
                <hr class="mb-4">

                <center>

                    <u>
                        <h4>KWITANSI</h4>
                    </u>
                    <h5>Surat Kuasa Untuk Membayar</h5>
                </center>

                <table>
                    <tr>
                        <td style="width:20px">a.</td>
                        <td style="width:150px">Nama Pemohon</td>
                        <td style="width:20px">:</td>
                        <td><?= $hasil['nama_pemohon'] ?></td>
                    </tr>
                    <tr>
                        <td style="width:20px">b.</td>
                        <td style="width:200px">Panjar Biaya Perkara</td>
                        <td style="width:20px">:</td>
                        <td>Rp. <?= number_format($result['biaya']) ?>, -</td>
                    </tr>
                    <tr>
                        <td style="width:20px">c.</td>
                        <td style="width:150px">Untuk Pembayaran</td>
                        <td style="width:20px">:</td>
                        <td>Panjar Biaya Perkara</td>
                    </tr>
                </table>

                <div class="row mt-4 mb-5">
                    <div class="col offset-md-9">
                        Kudus, <?= $hari_ini ?><br>
                        Pelayanan,
                        <br> <br> <br> <br>

                        <u><?= $hasil['nama'] ?></u><br>
                        NIP. <?= $hasil['NIP'] ?>
                    </div>
                </div>
                <h5>
                    Pembayaran ini dianggap sah apabila ada cap dan tanda tangan dari kasir
                </h5>
                <h5>CATATAN :</h5>
                <ul>
                    <li>Lembar I untuk Bank yang bersangkutan</li>
                    <li>Lembar II untuk Pemohon</li>
                    <li>Lembar III untuk Pelayanan</li>
                    <li>Lembar IV untuk dilampirkan dalam berkas</li>
                </ul>

                <center>
                    <a href="?menu=panjar" class="btn btn-info">Kembali</a>
                    <a href="panjar/cetak.php?id=<?php echo $kode_permohonan ?>" target="_blank" class="btn btn-primary" title="Cetak"><i class="mdi mdi-printer"></i></a>
                    <form method="post">
                        <input type="hidden" name="permohonan" value="<?= $kode_permohonan ?>">
                        <input type="hidden" name="total" value="<?= $result['biaya'] ?>">
                        <?php
                        if ($cekNomor2 == 0) :
                            ?>
                            <input type="submit" id="simpandb" name="simpandb" class="btn btn-success mt-2" value="Simpan">
                        <?php endif; ?>
                    </form>
                    <?php
                    $pm = $_POST['permohonan'];
                    $to = $_POST['total'];
                    if (isset($_POST['simpandb'])) {

                        $cek = $koneksi->query("INSERT INTO tbl_pembayaran VALUES(NULL, '$pm', NULL, '$to', 'belum', 'belum acc')");
                        if ($cek) { ?>
                            <script>
                                alert("Data berhasil Disimpan");
                                window.location.href = '?menu=cek&no=<?= $pm ?>'
                            </script>
                        <?php } else { ?>
                            <script>
                                alert("Data Gagal");
                                window.location.href = '?menu=panjar';
                            </script>
                        <?php }
                }

                ?>
                </center>
            </div>
        </div>
    </div>
</div>