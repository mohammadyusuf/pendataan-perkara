<div class="row">
    <div class="col-lg-9">
        <h1>
            Pelayanan
            <small>Pengadilan Agama</small>
        </h1>
    </div>
    <div class="col-lg-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-end">
                <li class="breadcrumb-item active"><a href="#">Pembayaran</a></li>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-striped" id="tabel">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Tgl Daftar</td>
                                <td>Jenis Pekara</td>
                                <td>Para Pihak</td>
                                <td>Aksi</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $data = mysqli_query($koneksi, "SELECT * FROM tbl_data_pihak as A INNER JOIN tbl_permohonan_perkara as B ON A.kode_data=B.kode_data INNER JOIN tbl_sub_perkara as C ON B.kode_sub=C.kode_sub");
                            $no = 1;
                            while ($p = mysqli_fetch_assoc($data)) :
                                ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $p['tgl_daftar_pemohon'] ?></td>
                                    <td><?= $p['nama_sub'] ?></td>
                                    <td>
                                        <?= $p['NIK_pemohon'] . " - " . $p['nama_pemohon'] ?> <br><br>
                                        <?= $p['nama_termohon'] == NULL ? "-" : $p['nama_termohon'] ?>
                                    </td>
                                    <td>
                                        <a href="?menu=cek&no=<?= $p['kode_permohonan'] ?>" class="btn btn-success">Cek</a>
                                    </td>
                                </tr>
                            <?php endwhile; ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>