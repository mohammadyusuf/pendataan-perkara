<div class="row">
    <div class="col-lg-9">
        <h1>
            Pelayanan
            <small>Pengadilan Agama</small>
        </h1>
    </div>
    <div class="col-lg-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-end">
                <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                <li class="breadcrumb-item active" aria-current="page">Biaya Panjar</li>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <a class="btn btn-rounded btn-primary mb-3 open" data-toggle="modal" data-target="#tPerkara"><i class="mdi mdi-plus"></i>Tambah</a>
                <div class="table-responsive">
                    <table class="table table-hover" id="tabel">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Radius</th>
                                <th>Sub Perkara</th>
                                <th>Biaya</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $p = $koneksi->query("SELECT * FROM tbl_panjar as A INNER JOIN tbl_sub_perkara as B ON A.kode_sub=B.kode_sub INNER JOIN tbl_radius as C ON A.kode_radius=C.kode_radius");
                            $no = 1;
                            while ($data = mysqli_fetch_assoc($p)) : ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $data['nama_radius']; ?></td>
                                    <td><?= $data['nama_sub']; ?></td>
                                    <td><?= $data['biaya']; ?></td>
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#ePerkara" data-id="<?= $data['kode_panjar'] ?>" data-sub="<?= $data['kode_sub'] ?>" data-radius="<?= $data['kode_radius'] ?>" data-biaya="<?= $data['biaya'] ?>" class="btn btn-dark btn-fw open_modal"><i class="mdi mdi-pencil"></i>Ubah</a>

                                        <a href="?menu=hapus_biaya&id=<?= $data['kode_panjar'] ?>" class="btn btn-danger btn-fw hapus" onclick="return confirm('Apakah anda yakin ingin menghapus?')"><i class="mdi mdi-delete"></i>Hapus</a>
                                    </td>
                                </tr>
                            <?php endwhile; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="tPerkara" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label">Tambah Biaya Panjar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="?menu=proses_biaya" method="POST">
                    <div class="form-group">
                        <label>Sub Perkara</label>
                        <select class="form-control" name="p" id="p">
                            <option value="">-- Pilih --</option>
                            <?php $k = $koneksi->query('SELECT * FROM tbl_sub_perkara');
                            while ($kec = mysqli_fetch_assoc($k)) :
                                ?>
                                <option value="<?= $kec['kode_sub'] ?>"><?= $kec['nama_sub'] ?></option>
                            <?php endwhile; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Radius</label>
                        <select class="form-control" name="ket" id="p">
                            <option value="">-- Pilih --</option>
                            <?php $k = $koneksi->query('SELECT * FROM tbl_radius');
                            while ($radius = mysqli_fetch_assoc($k)) :
                                ?>
                                <option value="<?= $radius['kode_radius'] ?>"><?= $radius['nama_radius'] ?></option>
                            <?php endwhile; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Biaya</label>
                        <input type="text" class="form-control" name="biaya">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                        <button type="submit" class="btn btn-success" name="tambah">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ePerkara" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label">Ubah Biaya Panjar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="?menu=proses_biaya" method="POST">
                    <div class="form-group">
                        <input type="hidden" id="idp" name="id">
                        <label>Sub Perkara</label>
                        <select class="form-control" name="p" id="sub">
                            <option value="">-- Pilih --</option>
                            <?php $k = $koneksi->query('SELECT * FROM tbl_sub_perkara');
                            while ($kec = mysqli_fetch_assoc($k)) :
                                ?>
                                <option value="<?= $kec['kode_sub'] ?>"><?= $kec['nama_sub'] ?></option>
                            <?php endwhile; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Radius</label>
                        <select class="form-control" name="ket" id="r">
                            <option value="">-- Pilih --</option>
                            <?php $k = $koneksi->query('SELECT * FROM tbl_radius');
                            while ($radius = mysqli_fetch_assoc($k)) :
                                ?>
                                <option value="<?= $radius['kode_radius'] ?>"><?= $radius['nama_radius'] ?></option>
                            <?php endwhile; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Biaya</label>
                        <input type="text" class="form-control" id="biaya" name="biaya">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                        <button type="submit" class="btn btn-success" name="ubah">Ubah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>