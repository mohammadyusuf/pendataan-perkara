<div class="row">
    <div class="col-lg-9">
        <h1>
            Pelayanan
            <small>Pengadilan Agama</small>
        </h1>
    </div>
    <div class="col-lg-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-end">
                <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                <li class="breadcrumb-item active" aria-current="page">Desa</li>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <a class="btn btn-rounded btn-primary mb-3 open" data-toggle="modal" data-target="#tPerkara"><i class="mdi mdi-plus"></i>Tambah</a>
                <button type="button" data-toggle="modal" data-target="#tambahUser" class="btn btn-outline-primary btn-icon-text ml-2">
                <i class="mdi mdi-account-plus"></i>
                Tambah Data
            </button>
                <div class="table-responsive">
                    <table class="table table-hover" id="tabel">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Desa</th>
                                <th>Kecamatan</th>
                                <th>Radius</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $p = $koneksi->query("SELECT * FROM tbl_kecamatan as A INNER JOIN tbl_desa as B ON A.kode_kecamatan=B.kode_kecamatan INNER JOIN tbl_radius as C ON B.kode_radius=C.kode_radius");
                            $no = 1;
                            while ($data = mysqli_fetch_assoc($p)) : ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $data['nama_desa']; ?></td>
                                    <td><?= $data['nama_kecamatan']; ?></td>
                                    <td><?= $data['nama_radius']; ?></td>
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#ePerkara" data-id="<?= $data['kode_desa'] ?>" data-desa="<?= $data['nama_desa'] ?>" data-kec="<?= $data['kode_kecamatan'] ?>" data-radius="<?= $data['kode_radius'] ?>" class="btn btn-dark btn-fw open_modal"><i class="mdi mdi-pencil"></i>Ubah</a>

                                        <a href="?menu=hapus_desa&id=<?= $data['kode_desa'] ?>" class="btn btn-danger btn-fw hapus" onclick="return confirm('Apakah anda yakin ingin menghapus?')"><i class="mdi mdi-delete"></i>Hapus</a>
                                    </td>
                                </tr>
                            <?php endwhile; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="tPerkara" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label">Tambah Desa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="?menu=proses_desa" method="POST">
                    <div class="form-group">
                        <label>Desa</label>
                        <input required class="form-control" type="text" id="s" name="s" />
                    </div>
                    <div class="form-group">
                        <label>Kecamatan</label>
                        <select class="form-control" name="p" id="p">
                            <option value="">-- Pilih --</option>
                            <?php $k = $koneksi->query('SELECT * FROM tbl_kecamatan');
                            while ($kec = mysqli_fetch_assoc($k)) :
                                ?>
                                <option value="<?= $kec['kode_kecamatan'] ?>"><?= $kec['nama_kecamatan'] ?></option>
                            <?php endwhile; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Radius</label>
                        <select class="form-control" name="ket" id="p">
                            <option value="">-- Pilih --</option>
                            <?php $k = $koneksi->query('SELECT * FROM tbl_radius');
                            while ($radius = mysqli_fetch_assoc($k)) :
                                ?>
                                <option value="<?= $radius['kode_radius'] ?>"><?= $radius['nama_radius'] ?></option>
                            <?php endwhile; ?>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                        <button type="submit" class="btn btn-success" name="tambah">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal Edit -->
<div class="modal fade" id="ePerkara" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label">Ubah Desa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="desa_form">
                    <input type="hidden" name="id" id="idk">
                    <div class="form-group">
                        <label>Desa</label>
                        <input required class="form-control" type="text" id="s" name="s" />
                    </div>
                    <div class="form-group">
                        <label>Kecamatan</label>
                        <select class="form-control" name="p" id="kec">
                            <option value="">-- Pilih --</option>
                            <?php $k = $koneksi->query('SELECT * FROM tbl_kecamatan');
                            while ($kec = mysqli_fetch_assoc($k)) :
                                ?>
                                <option value="<?= $kec['kode_kecamatan'] ?>"><?= $kec['nama_kecamatan'] ?></option>
                            <?php endwhile; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Radius</label>
                        <select class="form-control" name="ket" id="ket">
                            <option value="">-- Pilih --</option>
                            <?php $k = $koneksi->query('SELECT * FROM tbl_radius');
                            while ($radius = mysqli_fetch_assoc($k)) :
                                ?>
                                <option value="<?= $radius['kode_radius'] ?>"><?= $radius['nama_radius'] ?></option>
                            <?php endwhile; ?>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                        <button type="submit" class="btn btn-success" name="ubah">Ubah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>