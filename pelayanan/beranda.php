<?php
session_start();
if ($_SESSION['kode_user'] == "") {
  header("location:../index.php");
}
if ($_SESSION['level'] == "kasir") {
  header("location:../kasir/beranda.php");
}
if ($_SESSION['level'] == "kepala") {
  header("location:../kepala/beranda.php");
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Pendataan Perkara</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="../assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="../assets/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="../assets/vendors/css/vendor.bundle.addons.css">
  <link href="../assets/vendors/dataTable/dataTables.bootstrap4.min.css" rel="stylesheet" />
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" href="../assets/css/mystyle.css">
  <link rel="stylesheet" href="../assets/vendors/chosen-master/chosen.css">
  <link rel="stylesheet" href="../assets/jquery-ui/jquery-ui.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="../assets/images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html">
          <!-- <b>Pengadilan</b><small>Agama</small> -->
          <img src="../assets/images/logo.png" alt="logo" />
        </a>
        <a class="navbar-brand brand-logo-mini" href="index.html">
          <img src="../assets/images/bank.svg" alt="logo" />
        </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <ul class="navbar-nav navbar-nav-right">

          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <?php
              require '../config/koneksi.php';
              require '../config/fungsi.php';
              // $kode = kode('G');
              $user = $koneksi->query("SELECT * FROM tbl_user WHERE kode_user = '$_SESSION[kode_user]'");
              $detail = mysqli_fetch_array($user);
              ?>
              <span class="profile-text">Hello, <?= $detail['nama'] ?> !</span>
              <img class="img-xs rounded-circle" src="../assets/images/faces/face1.jpg" alt="Profile image">
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <a class="dropdown-item p-0">
                <div class="d-flex border-bottom">
                  <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                    <i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i>
                  </div>
                  <div class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
                    <i class="mdi mdi-account-outline mr-0 text-gray"></i>
                  </div>
                  <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                    <i class="mdi mdi-alarm-check mr-0 text-gray"></i>
                  </div>
                </div>
              </a>
              <a href="../logout.php" class="dropdown-item">
                Sign Out
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="user-wrapper">
                <div class="profile-image">
                  <img src="../assets/images/faces/face1.jpg" alt="profile image">
                </div>
                <div class="text-wrapper">
                  <p class="profile-name"><?= $detail['nama'] ?></p>
                  <div>
                    <small class="designation text-muted"><?= ucfirst($detail['status']) ?></small>
                    <span class="status-indicator online"></span>
                  </div>
                </div>
              </div>
              <i class="teal"></i>
            </div>
          </li>

          <?php
          @$menu = $_GET['menu'] ?>

          <li class="nav-item <?php if ($menu == "") {
                                echo 'active';
                              } ?>">
            <a class="nav-link" href="?menu">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">Beranda</span>
            </a>
          </li>
          <li class="nav-item <?php if ($menu == "perkara" || $menu == "desa" || $menu == "biayaP" || $menu == "user") {
                                echo 'active';
                              } ?>">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-content-copy"></i>
              <span class="menu-title">Master Data</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse <?php if ($menu == "kec" || $menu == "desa" || $menu == "biayaP"  || $menu == "user") {
                                    echo 'show';
                                  } ?>" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link <?php if ($menu == "kec") {
                                        echo 'active';
                                      } ?>" href="?menu=kec">Kecamatan</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link <?php if ($menu == "desa") {
                                        echo 'active';
                                      } ?>" href="?menu=desa">Desa</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link <?php if ($menu == "biayaP") {
                                        echo 'active';
                                      } ?>" href="?menu=biayaP">Biaya Panjar</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link <?php if ($menu == "user") {
                                        echo 'active';
                                      } ?>" href="?menu=user">User</a>
                </li>
              </ul>
            </div>
          </li>

          <?php
          $perkara = $koneksi->query('SELECT * FROM tbl_perkara_perdata');

          while ($p = $perkara->fetch_assoc()) : ?>
            <li class="nav-item <?php if ($menu == $p['kode_perkara']) {
                                  echo 'active';
                                } ?>">
              <a class="nav-link" href="?menu=<?= $p['kode_perkara'] ?>">
                <i class="menu-icon mdi mdi-scale-balance"></i>
                <span class="menu-title"><?= $p['nama_perkara'] ?></span>
              </a>
            <?php endwhile; ?>
          </li>

          <li class="nav-item <?php if ($menu == "panjar" || $menu == "cek") {
                                echo 'active';
                              } ?>">
            <a class="nav-link" href="?menu=panjar">
              <i class="menu-icon mdi mdi-cash"></i>
              <span class="menu-title">Cek Panjar</span>
            </a>
          </li>

        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">

          <?php

          error_reporting(E_NOTICE ^ E_ALL);
          switch ($_GET['menu']) {
            case 'kec':
              include 'kecamatan/index.php';
              break;
            case 'proses':
              include 'kecamatan/proses.php';
              break;
            case 'hapus':
              $id = $_GET['id'];
              $koneksi->query("DELETE FROM tbl_kecamatan WHERE kode_kecamatan = $id");
              echo "<script>window.location='?menu=kec'</script>";
              break;
            case 'desa':
              include 'desa/index.php';
              break;
            case 'proses_desa':
              include 'desa/proses_desa.php';
              break;
            case 'hapus_desa':
              $id = $_GET['id'];
              $koneksi->query("DELETE FROM tbl_desa WHERE kode_desa = $id");
              echo "<script>window.location='?menu=desa'</script>";
              break;
            case 'user':
              include 'user/index.php';
              break;
            case 'proses_user':
              include 'user/proses_user.php';
              break;
            case 'hapus_user':
              $id = $_GET['id'];
              $koneksi->query("DELETE FROM tbl_user WHERE kode_user = $id");
              echo "<script>window.location='?menu=user'</script>";
              break;

            case '1':
              include 'gugatan/index.php';
              break;
            case 'proses_data':
              include 'gugatan/proses.php';
              break;
            case '2':
              include 'permohonan/index.php';
              break;
            case '14':
              include 'syariah/index.php';
              break;
            case 'panjar':
              include 'panjar/index.php';
              break;
            case 'cek':
              include 'panjar/cek.php';
              break;
            case 'biayaP':
              include 'biaya/index.php';
              break;
            case 'proses_biaya':
              include 'biaya/proses_biaya.php';
              break;
            case 'hapus_biaya':
              $id = $_GET['id'];
              $koneksi->query("DELETE FROM tbl_panjar WHERE kode_panjar = $id");
              echo "<script>window.location='?menu=biayaP'</script>";
              break;
            default:
              include 'home.php';
              break;
          }
          ?>

        </div>

        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2019
              <a href="#" target="_blank">Pengadilan Agama Kudus</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
              <i class="mdi mdi-heart text-danger"></i>
            </span>
          </div>
        </footer>

      </div>
    </div>
  </div>

  <!-- plugins:js -->
  <script src="../assets/vendors/js/vendor.bundle.base.js"></script>
  <script src="../assets/vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="../assets/js/off-canvas.js"></script>
  <script src="../assets/js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="../assets/js/dashboard.js"></script>
  <!-- End custom js for this page-->
  <script src="../assets/js/jquery-3.1.1.min.js"></script>
  <script src="../assets/vendors/chosen-master/chosen.jquery.js"></script>
  <script src="../assets/vendors/dataTable/jquery.dataTables.min.js"></script>
  <script src="../assets/vendors/dataTable/dataTables.bootstrap4.min.js"></script>
  <script src="../assets/js/script.js"></script>
  <script src="../assets/js/panjar.js"></script>
  <script src="../assets/js/panjar2.js"></script>
  <script src="../assets/js/gugatan.js"></script>
  <script src="../assets/jquery-ui/jquery-ui.js"></script>
</body>

</html>