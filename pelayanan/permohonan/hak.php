<form class="form-sample" method="post">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Pemohon</label>
                <div class="col-sm-9">
                    <select class="form-control" name="pemohon" id="haPemohon">
                        <option value="">-- Pilih --</option>
                        <?php $cek = $koneksi->query("SELECT * FROM tbl_data_pihak WHERE status = 0");

                        while ($data = mysqli_fetch_assoc($cek)) : ?>

                            <option value="<?= $data['kode_data'] ?>"><?= $data['NIK_pemohon'] . " -" . $data['nama_pemohon'] ?></option>
                        <?php endwhile; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Tergugat</label>
                <div class="col-sm-9">
                    <input type="text" id="haTergugat" readonly name="tergugat" class="form-control" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <input type="hidden" name="tglD" value="<?= date('Y-m-d') ?>">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Tanggal Nikah</label>
                <div class="col-sm-9">
                    <input type="text" id="tgl-nikahHA" name="tglN" class="form-control" />
                </div>
            </div>
        </div>
    </div>

    <input type="submit" value="Simpan" name="simpanHA" class="btn btn-block btn-primary">
</form>

<?php

$kode_sub = 12;
$pemohon = $_POST['pemohon'];
$tglD = $_POST['tglD'];
$tglN = $_POST['tglN'];
$stok = 1;

if (isset($_POST['simpanHA'])) {

    $cek = $koneksi->query("INSERT INTO tbl_permohonan_perkara VALUES(NULL, '$kode_sub', '$pemohon', '$_SESSION[kode_user]', '$tglD', '$tglN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)");
    $update = $koneksi->query("UPDATE tbl_data_pihak SET status = '$stok' WHERE kode_data = '$pemohon'");
    if ($cek) { ?>
        <script>
            alert("Data berhasil Disimpan");
            window.location.href = '?menu=panjar';
        </script>
    <?php } else { ?>
        <script>
            alert("Data Gagal");
            window.location.href = '?menu=2';
        </script>
    <?php }
}
