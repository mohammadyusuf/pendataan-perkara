<form class="form-sample" method="post">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Pemohon</label>
                <div class="col-sm-9">
                    <select class="form-control" name="pemohon" id="wPemohon">
                        <option value="">-- Pilih --</option>
                        <?php $cek = $koneksi->query("SELECT * FROM tbl_data_pihak WHERE status = 0");

                        while ($data = mysqli_fetch_assoc($cek)) : ?>

                            <option value="<?= $data['kode_data'] ?>"><?= $data['NIK_pemohon'] . " -" . $data['nama_pemohon'] ?></option>
                        <?php endwhile; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <input type="hidden" name="tglD" value="<?= date('Y-m-d') ?>">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nama Pemohon II</label>
                <div class="col-sm-9">
                    <input type="text" name="pem2" class="form-control" />
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Pekerjaan Pemohon II</label>
                <div class="col-sm-9">
                    <input type="text" name="pekerjaan2" class="form-control" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nama Wali</label>
                <div class="col-sm-9">
                    <input type="text" name="wali" class="form-control" />
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Tanggal Nikah</label>
                <div class="col-sm-9">
                    <input type="text" id="tgl-nikahIN" name="tglN" class="form-control" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nama Saksi</label>
                <div class="col-sm-9">
                    <input type="text" name="saksi" class="form-control" />
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Mas Kawin</label>
                <div class="col-sm-9">
                    <input type="text" name="mas" class="form-control" />
                </div>
            </div>
        </div>
    </div>

    <input type="submit" value="Simpan" name="simpanIN" class="btn btn-block btn-primary">
</form>

<?php

$kode_sub = 14;
$pemohon = $_POST['pemohon'];
$tglD = $_POST['tglD'];
$tglN = $_POST['tglN'];
$wali = $_POST['wali'];
$pem2 = $_POST['pem2'];
$pekerjaan = $_POST['pekerjaan2'];
$saksi = $_POST['saksi'];
$mas = $_POST['mas'];
$stok = 1;

if (isset($_POST['simpanIN'])) {

    $cek = $koneksi->query("INSERT INTO tbl_permohonan_perkara VALUES(NULL, '$kode_sub', '$pemohon', '$_SESSION[kode_user]', '$tglD', '$tglN', NULL, NULL, NULL, NULL, '$mas', '$pem2', '$pekerjaan', '$wali', NULL, NULL, '$saksi')");
    $update = $koneksi->query("UPDATE tbl_data_pihak SET status = '$stok' WHERE kode_data = '$pemohon'");
    if ($cek) { ?>
        <script>
            alert("Data berhasil Disimpan");
            window.location.href = '?menu=panjar';
        </script>
    <?php } else { ?>
        <script>
            alert("Data Gagal");
            window.location.href = '?menu=2';
        </script>
    <?php }
}
