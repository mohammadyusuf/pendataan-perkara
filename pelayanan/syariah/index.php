<div class="row">
    <div class="col-lg-9">
        <h1>
            Pelayanan
            <small>Pengadilan Agama</small>
        </h1>
    </div>
    <div class="col-lg-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-end">
                <li class="breadcrumb-item"><a href="#">Permohonan</a></li>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div id="accordion" class="myaccordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    Data Penggugat dan Tergugat
                                    <span class="fa-stack fa-sm">
                                        <i class="fas fa-circle fa-stack-2x"></i>
                                        <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h2>
                        </div>
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <?php require "data_diri.php" ?>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#wali" aria-expanded="false" aria-controls="collapseOne">
                                    Gugatan Ekonomi Syariah
                                    <span class="fa-stack fa-sm">
                                        <i class="fas fa-circle fa-stack-2x"></i>
                                        <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h2>
                        </div>
                        <div id="wali" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <?php require "syariah.php" ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-striped" id="tabel">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Radius</td>
                                <td>Pemohon</td>
                                <td>No HP</td>
                                <td>Agama</td>
                                <td>Pekerjaan</td>
                                <td>Alamat</td>
                                <td>Tergugat</td>
                                <td>No HP</td>
                                <td>Agama</td>
                                <td>Pekerjaan</td>
                                <td>Alamat</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $data = mysqli_query($koneksi, "SELECT * FROM tbl_data_pihak as A INNER JOIN tbl_desa as B ON A.kode_desa=B.kode_desa INNER JOIN tbl_radius as C ON B.kode_radius=C.kode_radius");

                            $no = 1;
                            while ($p = mysqli_fetch_assoc($data)) :
                                ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $p['nama_radius'] ?></td>
                                    <td><?= $p['NIK_pemohon'] . " - " . $p['nama_pemohon'] ?></td>
                                    <td><?= $p['hp_pemohon'] ?></td>
                                    <td><?= $p['agama_pemohon'] ?></td>
                                    <td><?= $p['pekerjaan_pemohon'] ?></td>
                                    <td><?= $p['alamat_pemohon'] ?></td>
                                    <td><?= $p['nama_termohon'] == NULL ? "-" : $p['nama_termohon']  ?></td>
                                    <td><?= $p['hp_termohon'] == NULL ? "-" : $p['hp_termohon'] ?></td>
                                    <td><?= $p['agama_termohon'] == NULL ? "-" : $p['agama_termohon'] ?></td>
                                    <td><?= $p['pekerjaan_termohon'] == NULL ? "-" : $p['pekerjaan_termohon'] ?></td>
                                    <td><?= $p['alamat_termohon'] == NULL ? "-" : $p['alamat_termohon'] ?></td>
                                </tr>
                            <?php endwhile; ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>