<div class="row">
    <div class="col-lg-9">
        <h1>
            Pelayanan
            <small>Pengadilan Agama</small>
        </h1>
    </div>
    <div class="col-lg-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-end">
                <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                <li class="breadcrumb-item active" aria-current="page">User</li>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <a class="btn btn-rounded btn-primary mb-3 open" data-toggle="modal" data-target="#tPerkara"><i class="mdi mdi-plus"></i>Tambah</a>
                <div class="table-responsive">
                    <table class="table table-hover" id="tabel">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Jenis Kelamin</th>
                                <th>Alamat</th>
                                <th>NIP</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $p = $koneksi->query("SELECT * FROM tbl_user");
                            $no = 1;
                            while ($data = mysqli_fetch_assoc($p)) : ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $data['nama']; ?></td>
                                    <td><?= $data['jekel']; ?></td>
                                    <td><?= $data['alamat']; ?></td>
                                    <td><?= $data['NIP']; ?></td>
                                    <td>
                                        <?php
                                        $cek = $data['status'];
                                        if ($cek == "pelayanan") {
                                            echo "<span class='text-primary'>Pelayanan</span>";
                                        } elseif ($cek == "kasir") {
                                            echo "<span class='text-warning'>Kasir</span>";
                                        } else if ($cek == "kepala") {
                                            echo "<span class='text-success'>Kepala</span>";
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#ePerkara" data-nip="<?= $data['kode_user'] ?>" data-nama="<?= $data['nama'] ?>" data-jekel="<?= $data['jekel'] ?>" data-no="<?= $data['no_hp'] ?>" data-alamat="<?= $data['alamat'] ?>" data-username="<?= $data['NIP'] ?>" data-password="<?= $data['password'] ?>" data-status="<?= $data['status'] ?>" class="btn btn-dark open_modal"><i class="mdi mdi-pencil"></i>Ubah</a>

                                        <a href="?menu=hapus_user&id=<?= $data['kode_user'] ?>" class="btn btn-danger hapus" onclick="return confirm('Apakah anda yakin ingin menghapus?')"><i class="mdi mdi-delete"></i>Hapus</a>
                                    </td>
                                </tr>
                            <?php endwhile; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="tPerkara" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label">Tambah User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="?menu=proses_user" class="forms-sample">
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                        <div class="col-sm-9">
                            <input type="text" required name="nama" class="form-control" id="nama">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                        <div class="col-sm-9">
                            <select required class="form-control" name="p" id="p">
                                <option value="">-- Pilih --</option>
                                <option value="laki-laki">Laki-laki</option>
                                <option value="perempuan">Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no" class="col-sm-3 col-form-label">No Handphone</label>
                        <div class="col-sm-9">
                            <input type="text" required name="no" class="form-control" id="no">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                        <div class="col-sm-9">
                            <textarea name="alamat" required class="form-control" id="alamat" cols="10" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nip" class="col-sm-3 col-form-label">Username</label>
                        <div class="col-sm-9">
                            <input type="text" required name="user" id="user" class="form-control" id="nip">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-3 col-form-label">password</label>
                        <div class="col-sm-9">
                            <input type="password" required name="password" class="form-control" id="password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="status" class="col-sm-3 col-form-label">Status</label>
                        <div class="col-sm-9">
                            <select class="form-control" required name="status" id="status">
                                <option value="">-- Pilih --</option>
                                <option value="pelayanan">Pelayanan</option>
                                <option value="kasir">Kasir</option>
                                <option value="kepala">Kepala</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                        <button type="submit" class="btn btn-success" name="tambah">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal Edit -->
<div class="modal fade" id="ePerkara" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label">Ubah User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="user_form" class="forms-sample">
                    <div class="form-group row">
                        <input type="hidden" name="id" id="id">
                        <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                        <div class="col-sm-9">
                            <input type="text" required name="nama" class="form-control" id="nama">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                        <div class="col-sm-9">
                            <select required class="form-control" name="p" id="p">
                                <option value="">-- Pilih --</option>
                                <option value="laki-laki">Laki-laki</option>
                                <option value="perempuan">Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no" class="col-sm-3 col-form-label">No Handphone</label>
                        <div class="col-sm-9">
                            <input required type="text" name="no" class="form-control" id="no">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                        <div class="col-sm-9">
                            <textarea required name="alamat" class="form-control" id="alamat" cols="10" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-3 col-form-label">Username</label>
                        <div class="col-sm-9">
                            <input required type="user" name="user" class="form-control" id="user">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-9">
                            <input required type="text" name="password" class="form-control" id="pass">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="status" class="col-sm-3 col-form-label">Status</label>
                        <div class="col-sm-9">
                            <select required class="form-control" name="status" id="status">
                                <option value="">-- Pilih --</option>
                                <option value="pelayanan">Pelayanan</option>
                                <option value="kasir">Kasir</option>
                                <option value="kepala">Kepala</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                        <button type="submit" class="btn btn-success">Ubah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>