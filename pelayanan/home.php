<div class="row purchace-popup">
    <div class="col-5">
        <span class="d-block d-md-flex align-items-center">
            <p class="mr-2">Selamat Datang Petugas Pelayanan</p>
            <a class="btn purchase-button mt-4 mt-md-0" href="#"><?= $detail['nama'] ?></a>
            <i class="mdi mdi-close popup-dismiss d-none d-md-block"></i>
        </span>
    </div>
</div>

<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
            <div class="card-body">
                <div class="clearfix">
                    <div class="float-left">
                        <i class="mdi mdi-cube text-danger icon-lg"></i>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 text-right">Perkara Gugatan</p>
                        <?php $q = $koneksi->query('SELECT * FROM tbl_permohonan_perkara as B INNER JOIN tbl_sub_perkara as A ON B.kode_sub=A.kode_sub WHERE kode_perkara = 1');
                        $jml = $q->num_rows;
                        ?>
                        <div class="fluid-container">
                            <h3 class="font-weight-medium text-right mb-0"><?= $jml ?></h3>
                        </div>
                    </div>
                </div>
                <p class="text-muted mt-3 mb-0">
                    <a href="?menu=1">Selengkapnya </a><i class="mdi mdi-arrow-right mr-1" aria-hidden="true"></i>
                </p>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
            <div class="card-body">
                <div class="clearfix">
                    <div class="float-left">
                        <i class="mdi mdi-receipt text-warning icon-lg"></i>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 text-right">Perkara Permohonan</p>
                        <?php $q = $koneksi->query('SELECT * FROM tbl_permohonan_perkara as B INNER JOIN tbl_sub_perkara as A ON B.kode_sub=A.kode_sub WHERE kode_perkara = 2');
                        $jml = $q->num_rows;
                        ?>
                        <div class="fluid-container">
                            <h3 class="font-weight-medium text-right mb-0"><?= $jml ?></h3>
                        </div>
                    </div>
                </div>
                <p class="text-muted mt-3 mb-0">
                    <a href="?menu=2">Selengkapnya </a><i class="mdi mdi-arrow-right mr-1" aria-hidden="true"></i>
                </p>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
            <div class="card-body">
                <div class="clearfix">
                    <div class="float-left">
                        <i class="mdi mdi-poll-box text-success icon-lg"></i>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 text-right">Perkara Syariah</p>
                        <?php $q = $koneksi->query('SELECT * FROM tbl_permohonan_perkara as B INNER JOIN tbl_sub_perkara as A ON B.kode_sub=A.kode_sub WHERE kode_perkara = 14');
                        $jml = $q->num_rows;
                        ?>
                        <div class="fluid-container">
                            <h3 class="font-weight-medium text-right mb-0"><?= $jml ?></h3>
                        </div>
                    </div>
                </div>
                <p class="text-muted mt-3 mb-0">
                    <a href="?menu=14">Selengkapnya </a><i class="mdi mdi-arrow-right mr-1" aria-hidden="true"></i>
                </p>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
            <div class="card-body">
                <div class="clearfix">
                    <div class="float-left">
                        <i class="mdi mdi-account-location text-info icon-lg"></i>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 text-right">User</p>
                        <div class="fluid-container">
                            <?php $q = $koneksi->query('SELECT * FROM tbl_user');
                            $jml = $q->num_rows;
                            ?>
                            <h3 class="font-weight-medium text-right mb-0"><?= $jml ?></h3>
                        </div>
                    </div>
                </div>
                <p class="text-muted mt-3 mb-0">
                    <a href="?menu=user">Selengkapnya </a><i class="mdi mdi-arrow-right mr-1" aria-hidden="true"></i>
                </p>
            </div>
        </div>
    </div>
</div>