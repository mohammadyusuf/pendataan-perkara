<form class="form-sample" method="post">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Pemohon</label>
                <div class="col-sm-9">
                    <select class="form-control" name="pemohon" id="hPemohon">
                        <option value="">-- Pilih --</option>
                        <?php $cek = $koneksi->query("SELECT * FROM tbl_data_pihak WHERE status = 0");

                        while ($data = mysqli_fetch_assoc($cek)) : ?>

                            <option value="<?= $data['kode_data'] ?>"><?= $data['NIK_pemohon'] . " -" . $data['nama_pemohon'] ?></option>
                        <?php endwhile; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Tergugat</label>
                <div class="col-sm-9">
                    <input type="text" id="hTergugat" readonly name="tergugat" class="form-control" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <input type="hidden" name="tglD" value="<?= date('Y-m-d') ?>">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Tanggal Nikah</label>
                <div class="col-sm-9">
                    <input type="text" id="tgl-nikahH" name="tglN" class="form-control" />
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Harta Berupa</label>
                <div class="col-sm-9">
                    <textarea name="harta" cols="30" rows="5" class="form-control"></textarea>
                </div>
            </div>
        </div>
    </div>

    <input type="submit" value="Simpan" name="simpanHB" class="btn btn-block btn-primary">
</form>

<?php

$kode_sub = 6;
$pemohon = $_POST['pemohon'];
$tglD = $_POST['tglD'];
$tgln = $_POST['tglN'];
$harta = $_POST['harta'];
$status = 1;

if (isset($_POST['simpanHB'])) {

    $cek = $koneksi->query("INSERT INTO tbl_permohonan_perkara VALUES(NULL, '$kode_sub', '$pemohon', '$_SESSION[kode_user]', '$tglD', '$tgln', NULL, NULL, NULL, NULL, '$harta', NULL, NULL, NULL, NULL, NULL, NULL)");
    $update = $koneksi->query("UPDATE tbl_data_pihak SET status = '$status' WHERE kode_data = '$pemohon'");
    if ($cek) { ?>
        <script>
            alert("Data berhasil Disimpan");
            window.location.href = '?menu=panjar';
        </script>
    <?php } else { ?>
        <script>
            alert("Data Gagal");
            window.location.href = '?menu=1';
        </script>
    <?php }
}
