<form class="form-sample" method="post">
    <p class="card-description">
        <b>Data Termohon</b>
    </p>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">NIK Pemohon</label>
                <div class="col-sm-9">
                    <input type="text" name="nikp" class="form-control" />
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nama Pemohon</label>
                <div class="col-sm-9">
                    <input type="text" name="namap" class="form-control" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">NO HP</label>
                <div class="col-sm-9">
                    <input type="number" name="nop" class="form-control" />
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Umur Pemohon</label>
                <div class="col-sm-9">
                    <input type="text" name="umurp" class="form-control" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Agama</label>
                <div class="col-sm-9">
                    <select name="agamap" class="form-control">
                        <option>-- Pilih --</option>
                        <?php $agama = ['islam', 'kristen', 'katolik', 'budha', 'hindu', 'konghucu'];
                        foreach ($agama as $key) : ?>
                            <option value="<?= $key ?>"><?= ucfirst($key) ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Pekerjaan</label>
                <div class="col-sm-9">
                    <input type="text" name="pekerjaanp" class="form-control" />
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="bungkus">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Kecamatan</label>
                <div class="col-sm-3">
                    <select name="pendidikant" id="kec" class="form-control">
                    </select>
                </div>
                <label class="col-sm-2 col-form-label">Desa</label>
                <div class="col-sm-4">
                    <select name="kode_desa" id="desa" class="form-control">
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Radius</label>
                <div class="col-sm-6">
                    <input type="text" readonly class="form-control" id="radius">
                </div>
                <div class="col-sm-3">
                    <button id="busek" class="btn btn-danger btn-lg"><i class="mdi mdi-delete"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-end">
        <div class="col-6">
            <p class="text-danger font-italic">*pilih salah satu radius</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Pendidikan</label>
                <div class="col-sm-9">
                    <select name="pendidikanp" class="form-control">
                        <option>-- Pilih --</option>
                        <?php $sekolah = ['Tidak_sekolah', 'SD', 'SMP', 'SMA', 'D3', 'S1', 'S2'];
                        foreach ($sekolah as $key) : ?>
                            <option value="<?= $key ?>"><?= ucfirst($key) ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Alamat</label>
                <div class="col-sm-9">
                    <textarea name="alamatp" id="" cols="30" class="form-control" rows="5"></textarea>
                </div>
            </div>
        </div>
    </div>
    <p class="card-description">
        <b>Data Tergugat</b>
    </p>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nama Tergugat</label>
                <div class="col-sm-9">
                    <input type="text" name="namat" class="form-control" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">NO HP</label>
                <div class="col-sm-9">
                    <input type="text" name="not" class="form-control" />
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Umur Tergugat</label>
                <div class="col-sm-9">
                    <input type="text" name="umurt" class="form-control" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Agama</label>
                <div class="col-sm-9">
                    <select name="agamat" class="form-control">
                        <option>-- Pilih --</option>
                        <?php $agama = ['islam', 'kristen', 'katolik', 'budha', 'hindu', 'konghucu'];
                        foreach ($agama as $key) : ?>
                            <option value="<?= $key ?>"><?= ucfirst($key) ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Pekerjaan</label>
                <div class="col-sm-9">
                    <input type="text" name="pekerjaant" class="form-control" />
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="bungkus2">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Kecamatan</label>
                <div class="col-sm-3">
                    <select name="pendidikant" id="kec2" class="form-control">
                    </select>
                </div>
                <label class="col-sm-2 col-form-label">Desa</label>
                <div class="col-sm-4">
                    <select name="kode_desa" id="desa2" class="form-control">
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Radius</label>
                <div class="col-sm-6">
                    <input type="text" readonly class="form-control" id="radius2">
                </div>
                <div class="col-sm-3">
                    <button id="busek2" class="btn btn-danger btn-lg"><i class="mdi mdi-delete"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div id="bungkus2" class="row justify-content-end">
        <div class="col-6">
            <p class="text-danger font-italic">*pilih salah satu radius</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Pendidikan</label>
                <div class="col-sm-9">
                    <select name="pendidikant" class="form-control">
                        <option>-- Pilih --</option>
                        <?php $sekolah = ['Tidak_sekolah', 'SD', 'SMP', 'SMA', 'D3', 'S1', 'S2'];
                        foreach ($sekolah as $key) : ?>
                            <option value="<?= $key ?>"><?= ucfirst($key) ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Alamat</label>
                <div class="col-sm-9">
                    <textarea name="alamatt" id="" cols="30" class="form-control" rows="5"></textarea>
                </div>
            </div>
        </div>
    </div>

    <input type="submit" value="Simpan" name="simpan" class="btn btn-block btn-primary">
</form>

<?php

$nikp = $_POST['nikp'];
$desa = $_POST['kode_desa'];
$namap = $_POST['namap'];
$nop = $_POST['nop'];
$umurp = $_POST['umurp'];
$agamap = $_POST['agamap'];
$pekerjaanp = $_POST['pekerjaanp'];
$pendidikanp = $_POST['pendidikanp'];
$alamatp = $_POST['alamatp'];
$namat = $_POST['namat'];
$not = $_POST['not'];
$umurt = $_POST['umurt'];
$agamat = $_POST['agamat'];
$pekerjaant = $_POST['pekerjaant'];
$pendidikant = $_POST['pendidikant'];
$alamatt = $_POST['alamatt'];
$status = 0;

if (isset($_POST['simpan'])) {

    $query = "INSERT INTO tbl_data_pihak VALUES('', '$desa', '$nikp', '$namap', '$nop', '$umurp', '$agamap', '$pekerjaanp', '$pendidikanp', '$alamatp', '$namat', '$not', '$umurt','$pekerjaant', '$pendidikant', '$alamatt', '$agamat', '$status')";

    $cek = $koneksi->query($query);
    if ($cek) { ?>
        <script>
            alert("Data berhasil Disimpan");
            window.location.href = '?menu=1';
        </script>
    <?php } else { ?>
        <script>
            alert("Data Gagal");
            window.location.href = '?menu=1';
        </script>
    <?php }
}
