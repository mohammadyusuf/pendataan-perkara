<div class="row">
    <div class="col-lg-9">
        <h1>
            Pelayanan
            <small>Pengadilan Agama</small>
        </h1>
    </div>
    <div class="col-lg-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-end">
                <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                <li class="breadcrumb-item active" aria-current="page">Kecamatan</li>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <a class="btn btn-rounded btn-primary mb-3 open" data-toggle="modal" data-target="#tPerkara"><i class="mdi mdi-plus"></i>Tambah</a>
                <div class="table-responsive">
                    <table class="table table-hover" id="tabel">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kecamatan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $p = $koneksi->query("SELECT * FROM tbl_kecamatan");
                            $no = 1;
                            while ($data = mysqli_fetch_assoc($p)) : ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $data['nama_kecamatan']; ?></td>
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#ePerkara" data-id="<?= $data['kode_kecamatan'] ?>" data-nama="<?= $data['nama_kecamatan'] ?>" class="btn btn-dark btn-fw open_modal"><i class="mdi mdi-pencil"></i>Ubah</a>

                                        <a href="?menu=hapus&id=<?= $data['kode_kecamatan'] ?>" class="btn btn-danger btn-fw hapus" onclick="return confirm('Apakah anda yakin ingin menghapus?')"><i class="mdi mdi-delete"></i>Hapus</a>
                                    </td>
                                </tr>
                            <?php endwhile; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="tPerkara" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label">Tambah Kecamatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="?menu=proses" method="POST">
                    <div class=" form-group">
                        <label>Kecamatan</label>
                        <input required class="form-control" type="text" id="perkara" name="p" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                        <button type="submit" class="btn btn-success" name="tambah">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal Edit -->
<div class="modal fade" id="ePerkara" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label">Ubah Kecamatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="perkara_form">
                    <div class="form-group">
                        <label>Kecamatan</label>
                        <input type="hidden" id="idh" name="id">
                        <input required class="form-control" type="text" id="keca" name="p" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                        <button type="submit" class="btn btn-success" name="ubah">Ubah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>