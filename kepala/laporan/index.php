<?php include 'header.php' ?>
<div class="content-wrapper">

    <div class="row">
        <div class="col-lg-9">
            <h1>
                Kepala
                <small>Pengadilan Agama</small>
            </h1>
        </div>
        <div class="col-lg-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb justify-content-end">
                    <li class="breadcrumb-item active"><a href="#">Laporan</a></li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form action="" method="GET">
                        <label for="">Filter Berdasarkan</label>
                        <select name="filter" id="filter" class="form-control">
                            <option>-- Pilih --</option>
                            <option value="1">Per Bulan</option>
                            <option value="2">Per Tahun</option>
                        </select>
                        <br>
                        <div class="" id="bulan">
                            <label>Bulan</label>
                            <select name="bulan" class="form-control">
                                <option value="">-- Pilih --</option>
                                <option value="1">Januari</option>
                                <option value="2">Februari</option>
                                <option value="3">Maret</option>
                                <option value="4">April</option>
                                <option value="5">Mei</option>
                                <option value="6">Juni</option>
                                <option value="7">Juli</option>
                                <option value="8">Agustus</option>
                                <option value="9">September</option>
                                <option value="10">Oktober</option>
                                <option value="11">November</option>
                                <option value="12">Desember</option>
                            </select>
                        </div>

                        <div id="tahun">
                            <label>Tahun</label><br>
                            <select name="tahun" class="form-control">
                                <option value="">-- Pilih --</option>
                                <?php
                                $query = "SELECT YEAR(b.tgl_daftar_pemohon) as tahun FROM tbl_pembayaran as a INNER JOIN tbl_permohonan_perkara as b ON a.kode_permohonan=b.kode_permohonan GROUP BY YEAR(b.tgl_daftar_pemohon)"; // Tampilkan tahun sesuai di tabel Perkara
                                $sql = mysqli_query($koneksi, $query); // Eksekusi/Jalankan query dari variabel $query
                                while ($data = mysqli_fetch_assoc($sql)) : ?>
                                    <option value="<?= $data['tahun'] ?>"><?= $data['tahun'] ?></option>
                                <?php endwhile; ?>
                            </select>
                            <br />
                        </div>
                        <button class="btn btn-primary" type="submit">Tampilkan</button>
                        <a href="index.php" class="btn btn-warning">Reset Filter</a>
                        <br><br>
                    </form>

                    <?php
                    if (isset($_GET['filter'])) {
                        $filter = $_GET['filter'];

                        if ($filter == '1') {
                            $nama_bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                            echo '<b>Data Perkara Bulan ' . $nama_bulan[$_GET['bulan']] . ' ' . @$_GET['tahun'] . '</b><br /><br />';
                            $query = "SELECT L.acc, L.kode_pembayaran, L.nomor_perkara, B.tgl_daftar_pemohon, C.nama_sub, A.NIK_pemohon, A.nama_pemohon, A.nama_termohon FROM tbl_data_pihak as A INNER JOIN tbl_permohonan_perkara as B ON A.kode_data=B.kode_data INNER JOIN tbl_sub_perkara as C ON B.kode_sub=C.kode_sub INNER JOIN tbl_pembayaran as L ON B.kode_permohonan=L.kode_permohonan WHERE nomor_perkara AND MONTH(B.tgl_daftar_pemohon)='" . $_GET['bulan'] . "' AND YEAR(B.tgl_daftar_pemohon)='" . @$_GET['tahun'] . "'";
                        } else {
                            echo '<b>Data Perkara Tahun ' . @$_GET['tahun'] . '</b><br /><br />';;
                            $query = "SELECT L.acc, L.kode_pembayaran, L.nomor_perkara, B.tgl_daftar_pemohon, C.nama_sub, A.NIK_pemohon, A.nama_pemohon, A.nama_termohon FROM tbl_data_pihak as A INNER JOIN tbl_permohonan_perkara as B ON A.kode_data=B.kode_data INNER JOIN tbl_sub_perkara as C ON B.kode_sub=C.kode_sub INNER JOIN tbl_pembayaran as L ON B.kode_permohonan=L.kode_permohonan WHERE nomor_perkara AND YEAR(B.tgl_daftar_pemohon) = '" . @$_GET['tahun'] . "'";
                        }
                    } else {
                        echo '<b>Semua Data Perkara</b><br /><br />';
                        $query = "SELECT * FROM tbl_data_pihak as A INNER JOIN tbl_permohonan_perkara as B ON A.kode_data=B.kode_data INNER JOIN tbl_sub_perkara as C ON B.kode_sub=C.kode_sub INNER JOIN tbl_pembayaran as L ON B.kode_permohonan=L.kode_permohonan WHERE nomor_perkara";
                    }
                    ?>

                    <div class=" table-responsive">
                        <table class="table table-hover table-striped" id="tabel9">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Nomor Perkara</td>
                                    <td>Tgl Daftar</td>
                                    <td>Jenis Pekara</td>
                                    <td>Para Pihak</td>
                                    <td>ACC</td>
                                    <td>Aksi</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $data = mysqli_query($koneksi, $query);
                                while ($p = mysqli_fetch_assoc($data)) :
                                    ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td>
                                            <?= $p['nomor_perkara'] == NULL ? "-" : $p['nomor_perkara'] ?>
                                        </td>
                                        <td><?= $p['tgl_daftar_pemohon'] ?></td>
                                        <td><?= $p['nama_sub'] ?></td>
                                        <td>
                                            <?= $p['NIK_pemohon'] . " - " . $p['nama_pemohon'] ?> <br><br>
                                            <?= $p['nama_termohon'] == NULL ? "-" : $p['nama_termohon'] ?>
                                        </td>
                                        <td>
                                            <?php if (@$p['acc'] == 'belum acc') { ?>
                                                <span class='text-warning' id='belum'>Belum ACC</span>
                                            <?php } else { ?>
                                                <span class='text-success' id='belum'>Sudah ACC</span>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <a href="acc.php?no=<?= $p['kode_pembayaran'] ?>" class="btn btn-success">ACC</a>
                                        </td>

                                    </tr>
                                <?php endwhile; ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'footer.php' ?>