<!-- content-wrapper ends -->
<!-- partial:partials/_footer.html -->
<footer class="footer">
    <div class="container-fluid clearfix">
        <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2019
            <a href="#" target="_blank">Pengadilan Agama Kudus</a>. All rights reserved.</span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
            <i class="mdi mdi-heart text-danger"></i>
        </span>
    </div>
</footer>
</div>
</div>

</div>

<!-- plugins:js -->
<script src="../../assets/vendors/js/vendor.bundle.base.js"></script>
<script src="../../assets/vendors/js/vendor.bundle.addons.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="../../assets/js/off-canvas.js"></script>
<script src="../../assets/js/misc.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="../../assets/js/dashboard.js"></script>
<!-- End custom js for this page-->
<script src="../../assets/js/jquery-3.1.1.min.js"></script>
<script src="../../assets/vendors/chosen-master/chosen.jquery.js"></script>
<script src="../../assets/vendors/dataTable/jquery.dataTables.min.js"></script>
<script src="../../assets/vendors/dataTable2/datatables.min.js"></script>
<script src="../../assets/js/script.js"></script>
<script src="../../assets/js/panjar.js"></script>
<script src="../../assets/js/panjar2.js"></script>
<script src="../../assets/js/gugatan.js"></script>
<script src="../../assets/jquery-ui/jquery-ui.js"></script>
<script>
    $(document).ready(function() {
        $('#bulan, #tahun').hide()

        $('#filter').change(function() {
            if ($(this).val() == '1') {
                $('#bulan, #tahun').show()
            } else {
                $('#bulan').hide()
                $('#tahun').show()
            }
        })

        $('#tabel9').DataTable({
            dom: 'Bfrtip',
            buttons: [{
                    extend: 'pdf',
                    orientation: 'potrait',
                    pageSize: 'Legal',
                    title: 'Pendataan Perkara',
                    download: 'open',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    },
                },
                {
                    extend: 'excel',
                    title: 'Pendataan Perkara',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    },
                },
                {
                    extend: 'print',
                    title: 'Pendataan Perkara',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    },
                },
            ],
        });

    })
</script>
</body>

</html>