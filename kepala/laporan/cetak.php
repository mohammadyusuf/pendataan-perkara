<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Pendataan Perkara</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="../../assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../../assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="../../assets/vendors/css/vendor.bundle.addons.css">
    <link href="../assets/vendors/dataTable/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="../../assets/css/style.css">
    <link rel="stylesheet" href="../../assets/css/mystyle.css">
    <link rel="stylesheet" href="../../assets/vendors/chosen-master/chosen.css">
    <link rel="stylesheet" href="../../assets/jquery-ui/jquery-ui.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="../../assets/images/favicon.png" />
</head>

<body>

    <div class="main-panel">
        <div class="content-wrapper">
            <?php
            require '../../config/koneksi.php';
            require '../../config/fungsi.php';
            $kode_permohonan = $_GET['id'];
            $semua = $koneksi->query("SELECT A.nomor_perkara, C.nama_pemohon, C.umur_pemohon, C.alamat_pemohon, C.agama_pemohon, C.pekerjaan_pemohon FROM tbl_pembayaran as A INNER JOIN tbl_permohonan_perkara as B ON A.kode_permohonan=B.kode_permohonan INNER JOIN tbl_data_pihak as C ON B.kode_data=C.kode_data WHERE kode_pembayaran = $kode_permohonan");
            $hasil = mysqli_fetch_array($semua);

            $user = $koneksi->query("SELECT * FROM tbl_user WHERE kode_user = '$_SESSION[kode_user]'");
            $detail = mysqli_fetch_array($user);

            $hari_ini = tgl_indo(date('Y-m-d'));
            ?>

            <!-- <div class="row">
    <div class="col-lg-9">
        <h1>
            Pelayanan
            <small>Pengadilan Agama</small>
        </h1>
    </div>
    <div class="col-lg-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-end">
                <li class="breadcrumb-item"><a href="#">Pembayaran</a></li>
                <li class="breadcrumb-item active"><a href="#">Cek</a></li>
            </ol>
        </nav>
    </div>
</div> -->
            <style>
                hr {
                    display: block;
                    margin-top: 0.1em;
                    margin-bottom: 0.5em;
                    margin-left: auto;
                    margin-right: auto;
                    border-style: double;
                    border-width: 2px;
                }

                table,
                th,
                td {
                    padding: 8px 10px;
                }
            </style>
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="col-lg-10 offset-lg-1">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-1 mb-2">
                                        <img src="../../assets/images/logo8.png" width="100px" alt="">
                                    </div>
                                    <div class="col-md-11 mt-2">
                                        <center>
                                            <h3>PENGADILAN AGAMA KUDUS</h3>
                                            <h6>Jl. Raya Kudus - Pati KM 4 Kudus 59321</h6>
                                            <p>Telp/Fax : (0291) 438385</p>
                                        </center>
                                    </div>
                                </div>
                                <hr class="mb-4">

                                <center>


                                    <h4>BERKAS PUTUSAN</h4>
                                    <h4>PENERIMAAN PERMOHONAN PERKARA<h4>
                                </center>
                                <br><br>
                                <h5 style="text-indent: 60px; font-weight: normal; line-height: 2;">Pengadilan Agama Kudus yang memeriksa dan mengadili perkara-perkara, telah menetapkan putusan seperti tersebut di bawah ini dalam perkara terdakwa :</h5>
                                <table>
                                    <tr>
                                        <td style="width:50px"></td>
                                        <td style="width:150px">Nomor Perkara</td>
                                        <td style="width:20px">:</td>
                                        <td><?= $hasil['nomor_perkara'] ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width:20px"></td>
                                        <td style="width:200px">Nama Pemohon</td>
                                        <td style="width:20px">:</td>
                                        <td><?= $hasil['nama_pemohon'] ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width:20px"></td>
                                        <td style="width:150px">Umur</td>
                                        <td style="width:20px">:</td>
                                        <td><?= $hasil['umur_pemohon'] ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width:20px"></td>
                                        <td style="width:150px">Alamat</td>
                                        <td style="width:20px">:</td>
                                        <td><?= $hasil['alamat_pemohon'] ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width:20px"></td>
                                        <td style="width:150px">Agama</td>
                                        <td style="width:20px">:</td>
                                        <td><?= $hasil['agama_pemohon'] ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width:20px"></td>
                                        <td style="width:150px">Pekerjaan</td>
                                        <td style="width:20px">:</td>
                                        <td><?= $hasil['pekerjaan_pemohon'] ?></td>
                                    </tr>
                                </table>

                                <h5 style="text-indent: 60px; font-weight: normal; line-height: 2;">Berdasarkan dari hasil pertimbangan yang telah dilakukan, maka Pengadilan Agama Kudus memutuskan bahwa perkara tersebut telah terima Pengadilan Agama Kudus</h5>

                                <div class="row mt-4 mb-5">
                                    <div class="col offset-md-9">
                                        Kudus, <?= $hari_ini ?><br>
                                        Pimpinan,
                                        <br>
                                        <img width="100px" src="../../assets/images/12.png" alt="">
                                        <br>
                                        <u><?= $detail['nama'] ?></u><br>
                                        NIP. <?= $detail['NIP'] ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <script>
        window.print();
    </script>
    <!-- plugins:js -->
    <script src="../../assets/vendors/js/vendor.bundle.base.js"></script>
    <script src="../../assets/vendors/js/vendor.bundle.addons.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="../../assets/js/off-canvas.js"></script>
    <script src="../../assets/js/misc.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="../../assets/js/dashboard.js"></script>
    <!-- End custom js for this page-->
    <script src="../../assets/js/jquery-3.1.1.min.js"></script>
    <script src="../../assets/vendors/chosen-master/chosen.jquery.js"></script>
    <script src="../../assets/vendors/dataTable/jquery.dataTables.min.js"></script>
    <script src="../../assets/vendors/dataTable/dataTables.bootstrap4.min.js"></script>
    <script src="../../assets/js/script.js"></script>
    <script src="../../assets/js/panjar.js"></script>
    <script src="../../assets/js/panjar2.js"></script>
    <script src="../../assets/js/gugatan.js"></script>
    <script src="../../assets/jquery-ui/jquery-ui.js"></script>
</body>

</html>