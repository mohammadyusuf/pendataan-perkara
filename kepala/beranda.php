<?php
session_start();
require '../config/koneksi.php';
require '../config/fungsi.php';

if ($_SESSION['kode_user'] == "") {
    header("location:../index.php");
}
if ($_SESSION['level'] == "pelayanan") {
    header("location:../pelayanan/beranda.php");
}
if ($_SESSION['level'] == "kasir") {
    header("location:../kasir/beranda.php");
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Pendataan Perkara</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="../assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="../assets/vendors/css/vendor.bundle.addons.css">
    <link href="../assets/vendors/dataTable2/datatables.min.css" rel="stylesheet" />
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="../assets/css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="../assets/images/favicon.png" />
</head>

<body>
    <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
        <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
                <a class="navbar-brand brand-logo" href="index.html">
                    <!-- <b>Pengadilan</b><small>Agama</small> -->
                    <img src="../assets/images/logo.png" alt="logo" />
                </a>
                <a class="navbar-brand brand-logo-mini" href="index.html">
                    <img src="../assets/images/bank.svg" alt="logo" />
                </a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center">
                <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item dropdown d-none d-xl-inline-block">
                        <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                            <?php
                            $user = $koneksi->query("SELECT * FROM tbl_user WHERE kode_user = '$_SESSION[kode_user]'");
                            $detail = mysqli_fetch_array($user);
                            ?>
                            <span class="profile-text">Hello, <?= $detail['nama'] ?>!</span>
                            <img class="img-xs rounded-circle" src="../assets/images/faces/face1.jpg" alt="Profile image">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                            <a class="dropdown-item p-0">
                                <div class="d-flex border-bottom">
                                    <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                                        <i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i>
                                    </div>
                                    <div class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
                                        <i class="mdi mdi-account-outline mr-0 text-gray"></i>
                                    </div>
                                    <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                                        <i class="mdi mdi-alarm-check mr-0 text-gray"></i>
                                    </div>
                                </div>
                            </a>
                            <a href='../logout.php' class="dropdown-item">
                                Sign Out
                            </a>
                        </div>
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                    <span class="mdi mdi-menu"></span>
                </button>
            </div>
        </nav>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial:partials/_sidebar.html -->
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item nav-profile">
                        <div class="nav-link">
                            <div class="user-wrapper">
                                <div class="profile-image">
                                    <img src="../assets/images/faces/face1.jpg" alt="profile image">
                                </div>
                                <div class="text-wrapper">
                                    <p class="profile-name"><?= $detail['nama'] ?></p>
                                    <div>
                                        <small class="designation text-muted"><?= ucfirst($detail['status']) ?></small>
                                        <span class="status-indicator online"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <!-- <?php @$menu = $_GET['menu'] ?>

                    <li class="nav-item <?php if ($menu == "") {
                                            echo 'active';
                                        } ?>">
                        <a class="nav-link" href="?menu">
                            <i class="menu-icon mdi mdi-television"></i>
                            <span class="menu-title">Beranda</span>
                        </a>
                    </li>
                    <li class="nav-item <?php if ($menu == "laporan") {
                                            echo 'active';
                                        } ?>">
                        <a class="nav-link" href="laporan/index.php">
                            <i class="menu-icon mdi mdi-cash"></i>
                            <span class="menu-title">Laporan</span>
                        </a>
                    </li>
                    <li class="nav-item <?php if ($menu == "lg") {
                                            echo 'active';
                                        } ?>">
                        <a class="nav-link" href="../logout.php">
                            <i class="menu-icon mdi mdi-arrow-right"></i>
                            <span class="menu-title">Logout</span>
                        </a>
                    </li> -->

                    <li class="nav-item <?php if ($menu == "") {
                                            echo 'active';
                                        } ?>">
                        <a class="nav-link" href="beranda.php">
                            <i class="menu-icon mdi mdi-television"></i>
                            <span class="menu-title">Beranda</span>
                        </a>
                    </li>
                    <li class="nav-item <?php if ($menu == "laporan") {
                                            echo 'active';
                                        } ?>">
                        <a class="nav-link" href="laporan">
                            <i class="menu-icon mdi mdi-cash"></i>
                            <span class="menu-title">Laporan</span>
                        </a>
                    </li>
                    <li class="nav-item <?php if ($menu == "lg") {
                                            echo 'active';
                                        } ?>">
                        <a class="nav-link" href="../logout.php">
                            <i class="menu-icon mdi mdi-arrow-right"></i>
                            <span class="menu-title">Logout</span>
                        </a>
                    </li>

                </ul>
            </nav>
            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">

                    <div class="row purchace-popup">
                        <div class="col-5">
                            <span class="d-block d-md-flex align-items-center">
                                <p class="mr-2">Selamat Datang Pimpinan</p>
                                <a class="btn purchase-button mt-4 mt-md-0" href="#"><?= $detail['nama'] ?></a>
                                <i class="mdi mdi-close popup-dismiss d-none d-md-block"></i>
                            </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                            <div class="card card-statistics">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="mdi mdi-cube text-danger icon-lg"></i>
                                        </div>
                                        <div class="float-right">
                                            <p class="mb-0 text-right">Benomor Perkara</p>
                                            <?php $q = $koneksi->query("SELECT * FROM tbl_pembayaran WHERE nomor_perkara");
                                            $jml = $q->num_rows;
                                            ?>
                                            <div class="fluid-container">
                                                <h3 class="font-weight-medium text-right mb-0"><?= $jml ?></h3>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="text-muted mt-3 mb-0">
                                        <a href="laporan/index.php">Selengkapnya </a><i class="mdi mdi-arrow-right mr-1" aria-hidden="true"></i>
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- content-wrapper ends -->
                <!-- partial:partials/_footer.html -->
                <footer class="footer">
                    <div class="container-fluid clearfix">
                        <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2019
                            <a href="#" target="_blank">Pengadilan Agama Kudus</a>. All rights reserved.</span>
                        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                            <i class="mdi mdi-heart text-danger"></i>
                        </span>
                    </div>
                </footer>
            </div>
        </div>

    </div>

    <!-- plugins:js -->
    <script src="../assets/vendors/js/vendor.bundle.base.js"></script>
    <script src="../assets/vendors/js/vendor.bundle.addons.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="../assets/js/off-canvas.js"></script>
    <script src="../assets/js/misc.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="../assets/js/dashboard.js"></script>
    <!-- End custom js for this page-->
    <script src="../assets/js/jquery-3.1.1.min.js"></script>
    <script src="../assets/vendors/chosen-master/chosen.jquery.js"></script>
    <script src="../assets/vendors/dataTable/jquery.dataTables.min.js"></script>
    <script src="../assets/vendors/dataTable2/datatables.min.js"></script>
    <script src="../assets/js/script.js"></script>
    <script src="../assets/js/panjar.js"></script>
    <script src="../assets/js/panjar2.js"></script>
    <script src="../assets/js/gugatan.js"></script>
    <script src="../assets/jquery-ui/jquery-ui.js"></script>
    <script>
        $(document).ready(function() {
            $('#bulan, #tahun').hide()

            $('#filter').change(function() {
                if ($(this).val() == '1') {
                    $('#bulan, #tahun').show()
                } else {
                    $('#bulan').hide()
                    $('#tahun').show()
                }
            })

            $('#tabel9').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                        extend: 'pdf',
                        orientation: 'potrait',
                        pageSize: 'Legal',
                        title: 'Pendataan Perkara',
                        download: 'open',
                    },
                    'excel', 'print',
                ],
            });

        })
    </script>
</body>

</html>