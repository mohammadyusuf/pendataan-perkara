<?php
session_start();
require "config/koneksi.php";

if (isset($_SESSION['kode_user'])) {
  if ($_SESSION['level'] == 'pelayanan') {
    header("location:pelayanan/beranda.php");
  } elseif ($_SESSION['level'] == 'kasir') {
    header("location:kasir/beranda.php");
  } elseif ($_SESSION['level'] == 'kepala') {
    header("location:kepala/beranda.php");
  }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Halaman Login</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="assets/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="assets/images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
      <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="row mb-3">
              <div class="col">
                <img src="assets/images/logo8.png" style="float: left; margin-left: 1px" width="80px" alt="">
                <h2 style="float: left; margin-top: 20px; color: white" class="text-center"> <b>PENGADILAN </b>AGAMA</h2>
              </div>
            </div>
            <div class="auto-form-wrapper">
              <form action="index.php" method="post">
                <div class="form-group">
                  <label class="label">Username</label>
                  <div class="input-group">
                    <input type="text" class="form-control" required name="user" placeholder="Username">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-account"></i>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="label">Password</label>
                  <div class="input-group">
                    <input type="password" class="form-control" required name="pass" placeholder="*********">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-key"></i>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="submit" name="login" class="btn btn-primary submit-btn btn-block" value="Login">
                </div>
                <div class="form-group d-flex justify-content-between">

                  <!-- <p class="text-center">copyright © 2019 Pengadilan Agama Kudus. All rights reserved.</p> -->
                </div>

              </form>


              <?php

              if (isset($_POST['login'])) {
                $user = $_POST['user'];
                $pass = $_POST['pass'];
                $tampil = $koneksi->query("SELECT * FROM tbl_user WHERE NIP = '$user'");
                $cek = mysqli_fetch_assoc($tampil);

                if ($tampil->num_rows > 0) {
                  if ($cek['password'] == $pass) {
                    $_SESSION['kode_user'] = $cek['kode_user'];
                    $_SESSION['nama'] = $cek['nama'];

                    if ($cek['status'] == 'kasir') {
                      # code...
                      $_SESSION['level'] = $cek['status'];
                      header("location:kasir/beranda.php");
                    } else if ($cek['status'] == 'pelayanan') {
                      $_SESSION['level'] = $cek['status'];
                      header("location:pelayanan/beranda.php");
                    } else {
                      $_SESSION['level'] = $cek['status'];
                      header("location:kepala/beranda.php");
                    }
                  } else {
                    echo "<script>alert('Password yang anda masukkan salah');window.location.href = 'index.php';</script>";
                  }
                } else {
                  echo "<script>alert('Username yang anda masukkan salah');window.location.href = 'index.php';</script>";
                }
              }

              ?>

            </div>
            <p class="footer-text text-center mt-3">Copyright © 2019 Pengadilan Agama Kudus <br> All rights reserved.</p>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="assets/vendors/js/vendor.bundle.base.js"></script>
  <script src="assets/vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="assets/js/off-canvas.js"></script>
  <script src="assets/js/misc.js"></script>
  <!-- endinject -->
</body>

</html>