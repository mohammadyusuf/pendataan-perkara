<?php
include('koneksi.php');
$sql = "SELECT * FROM tbl_kecamatan";
$query = $koneksi->query($sql);
$data = array();
while ($row = $query->fetch_array(MYSQLI_ASSOC)) {
    $data[] = array("kode_kecamatan" => $row['kode_kecamatan'], "nama_kecamatan" => $row['nama_kecamatan']);
}
echo json_encode($data);
