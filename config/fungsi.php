<?php
include 'koneksi.php';

function kode($jenis)
{
    include('koneksi.php');
    $query = "SELECT max(nomor_perkara) as maxKode FROM tbl_pembayaran";
    $hasil = mysqli_query($koneksi, $query);
    $data = mysqli_fetch_array($hasil);
    $no_perkara = substr($data['maxKode'], 1, 3);

    $tahun = date('Y');

    $char = "/Pdt.{$jenis}/{$tahun}/PA.Kds";
    $tambah = $no_perkara + 1;
    // return $tambah;
    if ($tambah < 10) {
        return $id = "00" . $tambah . $char;
    } else {
        return $id = "0" . $tambah . $char;
    }
}

function tgl_indo($tanggal)
{
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
}
